import React from 'react';

import { Text, ScrollView, ImageBackground, SafeAreaView, TouchableOpacity, View } from "react-native";
import { InfoLink } from './info-link';
import {
    HelpingBackground,
    safari,
    mail,
    LogoIa,
    LogoNewEnvTrust,
    LogoImas,
    LogoAnds,
    LogoDaff,
    LogoUtas,
    LogoNsw,
    LogoDof,
    LogoPirsa,
    LogoUoa,
    LogoJcu,
    LogoMv,
    LogoIws,
    LogoHw,
} from "./images";

const styles = {
    imageBackground: {
        "flex": 1,
        "padding": 20
    },
    introText: {
        "color": "white",
        "paddingBottom": 20,
        "fontSize": 18
    },
    disclaimerText: {
        "color": "white",
        "paddingTop": 6,
        "paddingBottom": 8,
        "fontSize": 14,
        "textAlign": "center",
        "textDecorationLine": "underline",
        "textDecorationStyle": "solid",
        "textDecorationColor": "white"
    },
    headerView: {
        "padding": 10,
        "backgroundColor": "hsl(0,0%,50%)"
    },
    headerText: {
        "color": "white",
        "fontSize": 18
    },
    blockView: {
        paddingBottom: 10,
    }
}

export const InfoHeader = ({ children }) => (
    <View style={styles.headerView}>
        <Text style={styles.headerText}>{children}</Text>
    </View>
)

export const InfoBlock = ({ children }) => (
    <View style={styles.blockView}>
        {children}
    </View>
)

export const InfoLayout = ({
    onLinkPress
}) => (
        <ScrollView>
            <ImageBackground
                style={styles.imageBackground}
                resizeMode="cover"
                source={HelpingBackground}>
            <SafeAreaView></SafeAreaView>
            <Text style={styles.introText}>
                This Range Extension Database and Mapping project, known as
                Redmap, captures information that will help scientists assess
                how our marine ecosystems may be changing as our oceans warm.
            </Text>
                <Text style={styles.introText}>
                    Redmap invites you the fishers, divers and scientists of
                    Australia to Spot, Log and Map marine species that are unusual
                    or uncommon in particular areas along our coasts. The logging
                    process is now streamlined with the release of the Redmap App,
                    specifically designed to enable you to log your unusual sighting
                    anywhere.
            </Text>

                <InfoBlock>
                    <InfoHeader>
                        Visit the website and check out all the sightings in your
                        area and across Australia and to see our latest news and
                        fantastic resources here:
                </InfoHeader>
                    <InfoLink
                        onPress={() => onLinkPress("https://www.redmap.org.au")}
                        text="www.redmap.org.au"
                        icon={safari} />
                    <InfoLink
                        onPress={() => onLinkPress("mailto:enquiries@redmap.org.au")}
                        text="or contact the Redmap team via email"
                        icon={mail} />
                </InfoBlock>

                <InfoBlock>
                    <InfoHeader>
                        The development of this phone application was made possible
                        through the support of our sponsors and a generous grant
                        from:
                </InfoHeader>
                    <InfoLink
                        onPress={() => onLinkPress("https://www.innovation.gov.au/Science/InspiringAustralia/Pages/default.aspx")}
                        text="Inspiring Australia"
                        logo={LogoIa}
                        icon={safari} />
                    <InfoLink
                        onPress={() => onLinkPress("https://www.environment.nsw.gov.au/grants/envtrust.htm")}
                        text="NSW Environment Trust"
                        logo={LogoNewEnvTrust}
                        icon={safari} />
                </InfoBlock>

                <InfoBlock>
                    <InfoHeader>
                        Redmap Australia is supported by:
                    </InfoHeader>
                    <InfoLink
                        logo={LogoImas}
                        onPress={() => onLinkPress("https://www.imas.utas.edu.au/")}
                        text={"IMAS: the Institute for Marine and Antarctic Studies is the national host of Redmap"}
                        icon={safari} />
                    <InfoHeader>
                        National funders
                    </InfoHeader>
                    <InfoLink
                        logo={LogoIa}
                        onPress={() => onLinkPress("https://www.innovation.gov.au/Science/InspiringAustralia/Pages/default.aspx")}
                        text={"Inspiring Australia"}
                        icon={safari} />
                    <InfoLink
                        logo={LogoAnds}
                        onPress={() => onLinkPress("https://www.ands.org.au/")}
                        text={"Australian National Data Service"}
                        icon={safari} />
                    <InfoLink
                        logo={LogoDaff}
                        onPress={() => onLinkPress("http://www.daff.gov.au/")}
                        text={"Department of Agriculture, Fisheries and Forestry"}
                        icon={safari} />
                    <InfoHeader>
                        Tasmania
                    </InfoHeader>
                    <InfoLink
                        logo={LogoUtas}
                        onPress={() => onLinkPress("https://www.utas.edu.au/")}
                        text={"University of Tasmania"}
                        icon={safari} />
                    <InfoHeader>
                        New South Wales
                    </InfoHeader>
                    <InfoLink
                        logo={LogoNsw}
                        onPress={() => onLinkPress("https://www.newcastle.edu.au/")}
                        text={"University of Newcastle"}
                        icon={safari} />
                    <InfoHeader>
                        Western Australia
                    </InfoHeader>
                    <InfoLink
                        logo={LogoDof}
                        onPress={() => onLinkPress("https://www.fish.wa.gov.au/Pages/Home.aspx")}
                        text={"Department of Fisheries WA"}
                        icon={safari} />
                    <InfoHeader>
                        South Australia
                    </InfoHeader>
                    <InfoLink
                        logo={LogoPirsa}
                        onPress={() => onLinkPress("https://www.pir.sa.gov.au/")}
                        text={"Department of Primary Industries and Regions SA"}
                        icon={safari} />
                    <InfoLink
                        logo={LogoUoa}
                        onPress={() => onLinkPress("https://www.adelaide.edu.au/")}
                        text={"University of Adelaide"}
                        icon={safari} />
                    <InfoHeader>
                        Queensland
                    </InfoHeader>
                    <InfoLink
                        logo={LogoJcu}
                        onPress={() => onLinkPress("https://www.jcu.edu.au/")}
                        text={"James Cook University"}
                        icon={safari} />
                    <InfoHeader>
                        Victoria
                    </InfoHeader>
                    <InfoLink
                        logo={LogoMv}
                        onPress={() => onLinkPress("http://museumvictoria.com.au/")}
                        text={"Museum Victoria"}
                        icon={safari} />
                </InfoBlock>

                <InfoBlock>
                    <InfoHeader>
                        App development and design
                    </InfoHeader>
                    <InfoLink
                        logo={LogoIws}
                        onPress={() => onLinkPress("https://www.ionata.com.au/")}
                        text={"Ionata Web Solutions"}
                        icon={safari} />
                    <InfoLink
                        logo={LogoHw}
                        onPress={() => onLinkPress("http://hollywebber.com.au/")}
                        text={"Holly Webber"}
                        icon={safari} />
                </InfoBlock>

                <TouchableOpacity onPress={() => onLinkPress("https://www.redmap.org.au/misc/footer-links/legals/")}>
                    <Text style={styles.disclaimerText}>
                        Disclaimer
                    </Text>
                </TouchableOpacity>
                <SafeAreaView></SafeAreaView>

            </ImageBackground>
        </ScrollView>
    )