import React from 'react';
import { View, Text, ScrollView, RefreshControl, Image, Dimensions, ImageBackground, TouchableOpacity, Animated, SafeAreaView, Alert, useWindowDimensions } from 'react-native';
import { getDeviceSize, colours } from './theme';
import {
    RedMapHomeHero,
    UitableviewSeparator,
    HowAreYouHelpingBackground,
    HomeLoginOrCreateAccount as HomeLoginBackground,
    Lock,
    Lockdown,
    UitableviewcellDisclosureDefault,
    UitableviewcellDisclosureDefaultHl,
    UitableviewcellPersonal as MapIcon,
    UitableviewcellPersonalHl as MapIconWhite,
    UitableviewcellSpot,
    UitableviewcellSpotHl,
    UitableviewcellLog,
    UitableviewcellLogHl
} from './images'
import { Button } from './button';
import { OutboxBanner } from './banner';
import moment from 'moment';

const buttonViewStyles = ({ isHeld, screenWidth, colour }) => {

    const iconWidth =  screenWidth / 4;
    const iconHeight = screenWidth / 5;
    
    return {
        buttonViewContainer: { 
            flex: 0, 
            height: iconHeight, 
            paddingLeft: iconWidth, 
            flexDirection: "row", 
            backgroundColor: isHeld.interpolate({ inputRange: [0, 1], outputRange: ["white", colours.red] }) 
        },
        image1: { 
            flex: 0, 
            width: iconWidth, 
            height: iconHeight, 
            position: "absolute", 
            opacity: isHeld.interpolate({ inputRange: [0, 1], outputRange: [1, 0] }) 
        },
        image2: { 
            flex: 0, 
            width: iconWidth, 
            height: iconHeight, 
            position: "absolute", 
            opacity: isHeld 
        },
        textWrapper: {
            flex: 1,
            flexDirection: "column",
            justifyContent: "center",
            marginRight: screenWidth * 0.1,
        },
        titleText: { 
            fontSize: screenWidth / 17, 
            fontWeight: "bold", 
            color: isHeld.interpolate({ inputRange: [0, 1], outputRange: [colour, "white"] }) 
        },
        descText: { 
            fontSize:  screenWidth/22, 
            color: isHeld.interpolate({ inputRange: [0, 1], outputRange: ["hsl(0,0%,60%)", "white"] }) 
        },
        angleWrapper: { 
            height: iconHeight, 
            position: "absolute", 
            right: 0, 
            flexDirection: "row", 
            alignItems: "center", 
            opacity: isHeld.interpolate({ inputRange: [0, 1], outputRange: [1, 0] }) 
        },
        angleWhiteWrapper: { 
            height: iconHeight, 
            position: "absolute", 
            right: 0, 
            flexDirection: "row", 
            alignItems: "center", 
            opacity: isHeld 
        },
    }
};

const ButtonView = ({
    isHeld, icon, icon2, title, desc, colour, isLocked
}) => {
    const lock = Image.resolveAssetSource(Lock);
    const lockWhite = Image.resolveAssetSource(Lockdown);
    const angle = Image.resolveAssetSource(UitableviewcellDisclosureDefault);
    const angleWhite = Image.resolveAssetSource(UitableviewcellDisclosureDefaultHl);
    const screenWidth = Dimensions.get("window").width;
    const styles = buttonViewStyles({ isHeld, screenWidth, colour });
    const scaleFactor = screenWidth/320;
    return (
        <Animated.View style={styles.buttonViewContainer}>
            <Animated.Image
                style={styles.image1}
                resizeMode={"contain"}
                source={icon} />
            <Animated.Image
                style={styles.image2}
                resizeMode={"contain"}
                source={icon2} />
            <View style={styles.textWrapper}>
                <Animated.Text
                    style={styles.titleText}>
                    {title}
                </Animated.Text>
                <Animated.Text
                    style={styles.descText}>
                    {desc}
                </Animated.Text>
            </View>
            <Animated.View
                style={styles.angleWrapper}>
                {isLocked
                    ? <Image
                        style={{ flex: 0 }}
                        resizeMode={"contain"}
                        source={{ uri: lock.uri, width: lock.width * scaleFactor, height: lock.height * scaleFactor }} />
                    : null}
                <Image
                    style={{ flex: 0 }}
                    resizeMode={"contain"}
                    source={{ uri: angle.uri, width: angle.width * scaleFactor, height: angle.height * scaleFactor }} />
            </Animated.View>
            <Animated.View
                style={styles.angleWhiteWrapper}>
                {isLocked
                    ? <Image
                        style={{ flex: 0 }}
                        resizeMode={"contain"}
                        source={{ uri: lockWhite.uri, width: lockWhite.width * scaleFactor, height: lockWhite.height * scaleFactor }} />
                    : null}
                <Image
                    style={{ flex: 0 }}
                    resizeMode={"contain"}
                    source={{ uri: angleWhite.uri, width: angleWhite.width * scaleFactor, height: angleWhite.height * scaleFactor }} />
            </Animated.View>
        </Animated.View>
    )
}

const homeLayoutStyles = ({ screenWidth }) => {

    const scaleFactor = screenWidth/320;
    const heroData = Image.resolveAssetSource(RedMapHomeHero);

    return {
        refreshControl: { backgroundColor: "white" },
        mainContainer: { position: "absolute", top: 0, left: 0, right: 0, bottom: 0 },
        scrollView: { flex: 1, backgroundColor: "white" },
        scrollViewWrapper: { flex: 1, flexDirection: "column", backgroundColor: "white" },
        homeHeroView: { 
            flex: 0, 
            flexDirection: "row", 
            marginVertical: 5 * scaleFactor,
        },
        homeHeroImage: { 
            flex: 0, 
            width: screenWidth,
            height: screenWidth / heroData.width * heroData.height,
            marginVertical: 30 
        },
        menuWrapper: { flex: 1, flexDirection: "column" },
        menuImage: { width: screenWidth },
        helpWrapper: { flex: 0, flexDirection: "row", marginHorizontal: 20 },
        helpImageBackground: { flex: 1, justifyContent: "center", aspectRatio: (320 / 83) },
        helpText: { fontSize: screenWidth/9, fontFamily: "AlphaMack AOE", color: colours.red, paddingLeft: (40 * scaleFactor), textAlign: "left" },
        outboxBannerWrapper: {position: "absolute", top: 0, left: 0, right: 0 },
        footerWrapper: { position: "absolute", left: 0, right: 0, bottom: 0 },
        footerWrapperView: { flex: 0, flexDirection: "row" },
        footerImageBackround: { flex: 1, aspectRatio: (320 / 78) },
        footerTouchableOpacity: { top: "50%", width: "59%", height: "32%", justifyContent: "center", paddingLeft: "10%" },
        footerText: { color: "white", textAlign: "center", paddingHorizontal: 5, fontSize: screenWidth/32 },
        homeSafeAreaView: { backgroundColor: colours.footerBlue },
    }
}

const HomeLayout = ({
    isLoading, isAuthenticated, isRefreshing, outboxState, showRefreshSpinner, isInternetReachable, lastUpdated,
    onSpotPress, onLogPress, onMapPress, onHelpPress, onLoginPress, onLogoutPress, onRefresh
}) => {

    const screenWidth = Dimensions.get("window").width;
    const styles = homeLayoutStyles({ screenWidth });

    const handleLogout = () => {
        Alert.alert(
            "Are you sure you want to logout?",
            null,
            [
                { text: "No", style: "cancel" },
                { text: "Yes", onPress: () => onLogoutPress() }
            ]
        )
    }

    const handleRefresh = () => {
        onRefresh()
    }

    const handleFooterClick  = () => isAuthenticated ? handleLogout() : onLoginPress();
    const footerTextCopy = isAuthenticated ? "Log out" : "Log in or create an account";

    const lastUpdatedMoment = lastUpdated ? moment(lastUpdated) : null;
    const lastUpdatedString = lastUpdated && lastUpdatedMoment.isValid() ? "Last updated " + lastUpdatedMoment.fromNow() : "Refresh data";
    const refreshControlTitle = isRefreshing ? "Refreshing remote data..." : lastUpdatedString;

    return (
        <View
            style={styles.mainContainer}>
            <ScrollView
                style={styles.scrollView}
                refreshControl={
                    <RefreshControl
                        style={styles.refreshControl}
                        refreshing={showRefreshSpinner}
                        onRefresh={isInternetReachable && !isRefreshing ? handleRefresh : null}
                        title={refreshControlTitle}
                        tintColor={"#333"}
                        titleColor={"#333"} />
                }>
                <View
                    style={styles.scrollViewWrapper}>
                    <View
                        style={styles.homeHeroView}>
                        <Image
                            style={styles.homeHeroImage}
                            resizeMode={"contain"}
                            source={RedMapHomeHero} />
                    </View>
                    <View
                        style={styles.menuWrapper}>
                        <Button
                            testID="SpotPress"
                            onPress={() => onSpotPress()}
                            animatedView={(isHeld) => (
                                ButtonView({
                                    isHeld: isHeld,
                                    icon: UitableviewcellSpot,
                                    icon2: UitableviewcellSpotHl,
                                    title: "Spot",
                                    desc: "Explore species of interest",
                                    colour: colours.darkBlue,
                                })
                            )} />
                        <Image
                            resizeMode={"repeat"}
                            source={UitableviewSeparator}
                            style={styles.menuImage} />
                        <Button
                            disabled={isLoading}
                            testID="LogPress"
                            onPress={() => onLogPress()}
                            animatedView={(isHeld) =>
                                ButtonView({
                                    isHeld: isHeld,
                                    icon: UitableviewcellLog,
                                    icon2: UitableviewcellLogHl,
                                    title: "Log",
                                    desc: "Log your sightings",
                                    colour: colours.red
                                })} />
                        <Image
                            resizeMode={"repeat"}
                            source={UitableviewSeparator}
                            style={styles.menuImage} />
                        <Button
                            disabled={isLoading}
                            testID="MapPress"
                            onPress={() => isAuthenticated ? onMapPress() : onLoginPress()}
                            animatedView={(isHeld) =>
                                ButtonView({
                                    isHeld: isHeld,
                                    icon: MapIcon,
                                    icon2: MapIconWhite,
                                    title: "My Redmap",
                                    desc: "See all your sightings",
                                    colour: colours.midBlue,
                                    isLocked: !isAuthenticated,
                                })} />
                        <Image
                            resizeMode={"repeat"}
                            source={UitableviewSeparator}
                            style={styles.menuImage} />
                    </View>
                    <View
                        style={styles.helpWrapper}>
                        <ImageBackground
                            style={styles.helpImageBackground}
                            resizeMode={"contain"}
                            source={HowAreYouHelpingBackground}>
                            <TouchableOpacity
                                testID="HelpPress"
                                onPress={() => onHelpPress()}>
                                <Text
                                    style={styles.helpText}>
                                    How can you help?
                                </Text>
                            </TouchableOpacity>
                        </ImageBackground>
                    </View>
                </View>
            </ScrollView>
            <View style={styles.outboxBannerWrapper}>
                <OutboxBanner outboxState={outboxState} />
            </View>
            <View
                style={styles.footerWrapper}>
                <View
                    style={styles.footerWrapperView}>
                    <ImageBackground
                        style={styles.footerImageBackround}
                        resizeMode={"cover"}
                        source={HomeLoginBackground}>
                        <TouchableOpacity
                            disabled={isLoading}
                            testID="FooterClick"
                            onPress={handleFooterClick}
                            style={styles.footerTouchableOpacity}>
                            <Text
                                style={styles.footerText}>
                                {footerTextCopy}
                            </Text>
                        </TouchableOpacity>
                    </ImageBackground>
                </View>
                <SafeAreaView
                    style={styles.homeSafeAreaView} />
            </View>
        </View>
    );
}

export { HomeLayout };
