import React from 'react';
import { storiesOf } from '@storybook/react-native';
import { withKnobs, boolean, text } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import { View } from 'react-native';
import { LabelledInput, UsernameInput, PasswordInput, EmailInput, FamilyNameInput, GivenNameInput } from './labelled-input';

const stories = storiesOf('Labelled Inputs', module).addDecorator(withKnobs);

const label = text("label", "Field label")
const placeholder = text("placeholder", "Placeholder text")
const onChangeText = action("onChangeText");
const value = text("value", "");
const hasError = boolean("hasError", false);
const inputProps = {};


const Story1 = () => {

    const label = text("label", "Field label")
    const placeholder = text("placeholder", "Placeholder text")
    const onChangeText = action("onChangeText");
    const value = text("value", "");
    const hasError = boolean("hasError", false);
    const inputProps = {};

    return (
        <View style={{ padding: 50 }}>
            <LabelledInput {...{ label, placeholder, onChangeText, value, hasError, inputProps }} />
        </View>
    )
}

stories.add('Basic input', () => (<Story1 />));


const Story2 = () => {

    const placeholder = text("placeholder", "Placeholder text")
    const onChangeText = action("onChangeText");
    const value = text("value", "");
    const hasError = boolean("hasError", false);

    return (
        <View style={{ padding: 50 }}>
            <UsernameInput {...{ label: "Username", placeholder, onChangeText, value, hasError }} />
            <PasswordInput {...{ label: "Password", placeholder, onChangeText, value, hasError }} />
            <EmailInput {...{ label: "Email", placeholder, onChangeText, value, hasError }} />
            <FamilyNameInput {...{ label: "Family Name", placeholder, onChangeText, value, hasError }} />
            <GivenNameInput {...{ label: "Given Name", placeholder, onChangeText, value, hasError }} />
        </View>
    )
}

stories.add('Configured inputs', () => (<Story2 />));
