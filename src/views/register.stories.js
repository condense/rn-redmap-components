import React from 'react';
import { storiesOf } from '@storybook/react-native';
import { withKnobs, text, boolean } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import { RegisterScreen } from './register';

const stories = storiesOf('Register Screen', module).addDecorator(withKnobs);

stories.add('RegisterScreen', () => {
    const values = {
        username: text("username value", "", "Values"),
        password: text("password value", "", "Values"),
        first_name: text("first_name value", "", "Values"),
        last_name: text("last_name value", "", "Values"),
        email: text("email value", "", "Values"),
        join_mailing_list: boolean("join_mailing_list value", false, "Values"),
        region: text("region value", null, "Values"),
    } 
    const errors = {
        username: boolean("username error?", false, "Errors"),
        password: boolean("password error?", false, "Errors"),
        first_name: boolean("first_name error?", false, "Errors"),
        last_name: boolean("last_name error?", false, "Errors"),
        email: boolean("email error?", false, "Errors"),
        join_mailing_list: boolean("join_mailing_list error?", false, "Errors"),
        region: boolean("region error?", false, "Errors"),
    } 
    return (
        <RegisterScreen
            isInternetReachable={boolean("isInternetReachable", true)}
            values={values}
            errors={errors}
            onValueChange={action("onValueChange")}
            onRegionPress={action("onRegionPress")}
        />
    )
});
