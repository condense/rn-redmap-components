
import React, { useRef } from "react";
import { View, Text, Animated } from 'react-native';
import { colours } from './theme';

const styles = {
    view: {
        padding: 5,
        backgroundColor: colours.midBlue,
    },
    text: {
        color: "white",
        textAlign: "center",
        paddingHorizontal: 5,
    }
}

export const Banner = ({ show, message }) => {

    const fadeAnim = useRef(new Animated.Value(0)).current;

    const fadeIn = () => {
        Animated.timing(fadeAnim, {
            toValue: 1,
            duration: 1000,
            useNativeDriver: true
        }).start();
    };

    const fadeOut = () => {
        // Will change fadeAnim value to 0 in 5 seconds
        Animated.timing(fadeAnim, {
            toValue: 0,
            duration: 1000,
            useNativeDriver: true
        }).start();
    };

    show ? fadeIn() : fadeOut();

    return (

        <Animated.View
            style={{ opacity: fadeAnim }}
        >
            <View style={styles.view}>
                <Text style={styles.text}>
                    {message}
                </Text>
            </View>
        </Animated.View>

    )
}

export const ConnectionBanner = ({ isInternetReachable }) => (
    <Banner
        show={!isInternetReachable}
        message="Can't connect to our server" />
)

export const SubmittingBanner = ({ isSubmitting }) => (
    <Banner
        show={isSubmitting}
        message="Sending sighting..."
    />
)

const messages = {
    empty: "Outbox is empty",
    submitting: "Sending your sighting...",
    queued: "Waiting to send sighting",
}

const outboxStyles = ({fadeAnim}) => ({
    view: {
        padding: 5,
        backgroundColor: colours.midBlue,
        transform: [{
            translateY: fadeAnim.interpolate({
              inputRange: [0, 0.5, 1],
              outputRange: [-30, 0, 0]
            }),
          }],          
    },
    text: {
        color: "white",
        textAlign: "center",
        paddingHorizontal: 5,
        opacity: fadeAnim.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [0, 0, 1]
          }),
    }
})

export const OutboxBanner = ({ outboxState }) => {

    const fadeAnim = useRef(new Animated.Value(outboxState=="empty" ? 0: 1)).current;

    const fadeIn = () => {
      Animated.timing(fadeAnim, {
        toValue: 1,
        duration: 500,
        useNativeDriver: true
      }).start();
    };
  
    const fadeOut = () => {
      Animated.timing(fadeAnim, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true
      }).start();
    };

    if (outboxState=="empty") { fadeOut() } else { fadeIn() }

    let styles = outboxStyles({fadeAnim})
    let message = messages[outboxState]

    return (
        <Animated.View style={styles.view}>
            <Animated.Text style={styles.text}>
                {message}
            </Animated.Text>
        </Animated.View>
    )
}