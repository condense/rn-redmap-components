
import React from 'react';
import { View, Animated, Text, KeyboardAvoidingView, Switch } from 'react-native';
import { FormPage } from './form-page';
import { UsernameInput, PasswordInput, GivenNameInput, FamilyNameInput, EmailInput, ErrorBadge } from './labelled-input';
import { Button } from './button';
import { ConnectionBanner } from './banner';
import { Icon } from 'react-native-elements';

const copy = {
    usernameInfoCopy: "The username should be between 3 and 30 characters: letters, numbers and @ , + - _",
    passwordInfoCopy: "the password has a minimum of 6 characters",
}

const styles = {
    accountFormGroup: {
        "borderBottomWidth": 1,
        "borderBottomColor": "hsl(0,0%,70%)"
    },
    infoFormGroup: {
        "borderBottomWidth": 1,
        "borderBottomColor": "hsl(0,0%,70%)"
    },
    accountText: {
        padding: 10,
        color: "hsl(0,0%,30%)"
    },
    infoView: {
        "margin": 10
    },
    infoText: {
        "color": "hsl(0,0%,30%)"
    },
    profileText: {
        "padding": 10,
        "marginTop": 10,
        "color": "hsl(0,0%,30%)"
    },
    joinView: {
        "flexDirection": "row",
        "justifyContent": "space-between",
        "alignItems": "center",
        "backgroundColor": "white",
        "padding": 10,
        "borderBottomWidth": 1,
        "borderBottomColor": "hsl(0,0%,70%)"
    },
    joinText: {
        "fontSize": 16,
        "fontWeight": "bold"
    }
};

const regionButtonStyles = ({ isHeld }) => ({
    container: {
        "flexDirection": "row",
        "justifyContent": "space-between",
        "alignItems": "center",
        "backgroundColor": isHeld.interpolate({
            "inputRange": [0, 1],
            "outputRange": ["white", "hsl(0,0%,90%)"]
        }),
        "height": 52,
        "padding": 10,
        "borderBottomWidth": 1,
        "borderBottomColor": "hsl(0,0%,70%)"
    },
    label: {
        "fontSize": 16,
        "fontWeight": "bold"
    },
    valueView: {
        "flexDirection": "row",
        "alignItems": "center"
    },
})

const RegionButtonView = ({ isHeld, value, hasError }) => {
    const styles = regionButtonStyles({ isHeld });
    const valueText = value || "";
    return (
        <Animated.View style={styles.container}>
            <Text style={styles.label}>Region</Text>
            <View style={styles.valueView}>
                <Text>{valueText}</Text>
                <Icon name="chevron-right" color="hsl(0,0%,50%)" />
            </View>
            <ErrorBadge hasError={hasError} />
        </Animated.View>
    )
}

export const FormGroup = ({ style, children, ...props }) => (
    <View style={{ ...style, paddingTop: 40 }} {...props}>
        { children}
    </View>
)

export const RegisterScreen = ({
    values, errors, isInternetReachable, onValueChange, onRegionPress
}) => {
    return (
        <KeyboardAvoidingView>
            <FormPage>
                <ConnectionBanner isInternetReachable={isInternetReachable} />
                <FormGroup style={styles.accountFormGroup}>
                    <Text style={styles.accountText}>ACCOUNT DETAILS</Text>
                    <UsernameInput
                        label="Username"
                        onChangeText={s => onValueChange('username', s)}
                        value={values.username}
                        hasError={errors.username ? true : false}
                    />
                    <PasswordInput
                        label="Password"
                        onChangeText={s => onValueChange('password', s)}
                        value={values.password}
                        hasError={errors.password ? true : false}
                    />
                </FormGroup>
                <FormGroup style={styles.infoFormGroup}>
                    <View style={styles.infoView}>
                        <Text style={styles.infoText}>
                            {copy.usernameInfoCopy}
                        </Text>
                    </View>
                    <View style={styles.infoView}>
                        <Text style={styles.infoText}>
                            {copy.passwordInfoCopy}
                        </Text>
                    </View>
                    <Text style={styles.profileText}>
                        PROFILE
                    </Text>
                </FormGroup>
                <FamilyNameInput
                    label="First name"
                    onChangeText={s => onValueChange('first_name', s)}
                    value={values.first_name}
                    hasError={errors.first_name ? true : false}
                />
                <GivenNameInput
                    label="Last name"
                    onChangeText={s => onValueChange('last_name', s)}
                    value={values.last_name}
                    hasError={errors.last_name ? true : false}
                />
                <EmailInput
                    label="Email"
                    onChangeText={s => onValueChange('email', s)}
                    value={values.email}
                    hasError={errors.email ? true : false}
                />
                <View style={styles.joinView}>
                    <Text style={styles.joinText}>Join Mailing List?</Text>
                    <Switch
                        value={values.join_mailing_list}
                        onValueChange={v => onValueChange('join_mailing_list', v)}
                    />
                    <ErrorBadge hasError={(errors.join_mailing_list ? true : false)} />
                </View>
                <Button
                    style={styles.regionButton}
                    onPress={() => onRegionPress()}
                    animatedView={(isHeld) => (
                        RegionButtonView({
                            isHeld,
                            value: values.region,
                            hasError: (errors.region ? true : false)
                        })
                    )}
                />
            </FormPage>
        </KeyboardAvoidingView>
    )
}
