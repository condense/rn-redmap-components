
import React from 'react';
import { View, Text, TouchableWithoutFeedback, Image, Animated } from 'react-native';

const getStyles = (isHeld) => ({
    "wrapper": {
        "minHeight": 40,
        "paddingHorizontal": 10,
        "flexDirection": "row",
        "borderBottomColor": "hsl(0,0%,80%)",
        "borderBottomStyle": "solid",
        "borderBottomWidth": 1,
        "backgroundColor": isHeld.interpolate({
            inputRange: [0, 1],
            outputRange: ["white", "hsl(0,0%,90%)"]
        }),
        "alignItems": "center"
    },
    "logoView": {
        "flex": 1,
        "flexDirection": "row",
    },
    "logoImage": {
        "maxHeight": 30,
        "width": 50,
        "marginTop": 10,
        "marginRight": 10,
        "alignSelf": "flex-start"
    },
    "iconImage": {
        "width": 20,
        "height": 20
    },
    "linkView": {
        "flex": 1
    },
    "linkText": {
        "color": "black",
        "fontWeight": "bold",
        "paddingVertical": 15,
        "fontSize": 18
    }
})

export const InfoLink = ({ logo, text, icon, onPress }) => {
    const isHeld = React.useRef(new Animated.Value(0)).current;
    const styles = getStyles(isHeld);
    return (
        <TouchableWithoutFeedback
            style={styles.container}
            onPress={() => onPress()}
            onPressIn={() => isHeld.setValue(1)}
            onPressOut={() => isHeld.setValue(0)}>
            <Animated.View style={styles.wrapper}>
                <View style={styles.logoView}>
                    {logo
                        ? <Image
                            style={styles.logoImage}
                            resizeMode="contain"
                            source={logo} />
                        : null}
                    <View style={styles.linkView}>
                        <Text style={styles.linkText}>{text}</Text>
                    </View>
                </View>
                <Image 
                    style={styles.iconImage} 
                    resizeMode="contain"
                    source={icon} />
            </Animated.View>
        </TouchableWithoutFeedback>
    )
}
