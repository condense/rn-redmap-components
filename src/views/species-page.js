import React from 'react';
import { Image, SafeAreaView, ScrollView, Text, View, ActivityIndicator } from 'react-native';
import { colours, getFooterHeight, getScaleFactor } from './theme';
import { AustraliaMap } from './images';

const distributionMapStyles = ({ scaleFactor }) => {
    const size = 100 * scaleFactor;
    return {
        container: {
            width: size,
            height: size,
            flex: 0
        },
        baseImage: {
            "width": size,
            "height": size,
            "top": 0,
            "left": 0,
            "position": "absolute"
        },
        distImage: {
            "width": size,
            "height": size,
            "top": 0,
            "left": 0,
            "position": "absolute"
        },
    }
}

const DistributionMap = ({ distributionUrl }) => {
    const scaleFactor = getScaleFactor();
    const styles = distributionMapStyles({ scaleFactor });

    return (
        <View style={styles.container}>
            <Image
                source={AustraliaMap}
                resizeMode="contain"
                style={styles.baseImage}
            />
            <Image
                source={{ uri: distributionUrl }}
                resizeMode="contain"
                style={styles.distImage}
            />
        </View>
    )
}

// TODO: graceful pictureURL loading (e.g. keep space? show loader? adapt height?)

const PictureWithCaption = ({
    imageCredit, pictureUrl
}) => (
    <View style={{ flex: 1 }}>
        <View style={styles.imageRow}>
            <Image
                source={{ uri: pictureUrl }}
                resizeMode="contain"
                style={{
                    "flex": 0,
                    "width": "100%",
                    "aspectRatio": 1.3
                }} />
        </View>
        <View style={styles.row}>
            <Text style={styles.imageCreditText}>
                {"(Image credit: " + imageCredit + ")"}
            </Text>
        </View>
    </View>
)

export const SpeciesPage = ({
    speciesName, distributionUrl, notes, imageCredit, pictureUrl, description, commonName
}) => {

    return (
        <ScrollView style={styles.container}>
            <View style={styles.wrapper}>
                <View style={styles.row}>
                    <Text style={styles.commonNameText}>
                        {commonName}
                    </Text>
                </View>
                <View style={styles.row}>
                    <Text style={styles.speciesNameText}>
                        {speciesName}
                    </Text>
                </View>
                <PictureWithCaption
                    pictureUrl={pictureUrl}
                    imageCredit={imageCredit} />
                <View style={styles.pictureView}>
                    <View style={styles.row}>
                        <Text style={styles.noteTitleText}>Log it</Text>
                        <Text style={styles.noteCopyText}>{notes}</Text>
                    </View>
                    <DistributionMap
                        distributionUrl={distributionUrl} />
                </View>
                <View style={styles.descriptionTitleView}>
                    <Text style={styles.descriptionTitleText}>Species info</Text>
                </View>
                <View style={styles.row}>
                    <Text style={styles.descriptionCopyText}>{description}</Text>
                </View>
            </View>
            <SafeAreaView />
        </ScrollView>
    )
};

const styles = {
    container: {
        "flex": 1,
        "backgroundColor": "white"
    },
    wrapper: {
        padding: 20,
        paddingBottom: getFooterHeight()
    },
    pictureView: {
        "marginTop": 32,
        "flexDirection": "row"
    },
    row: {
        flex: 1
    },
    imageRow: {
        "flex": 1,
        "marginTop": 15,
        "marginBottom": 15
    },
    commonNameText: {
        "fontSize": 28,
        "fontWeight": "600"
    },
    speciesNameText: {
        "fontStyle": "italic",
        "fontSize": 20,
        "fontWeight": "200"
    },
    imageCreditText: {},
    noteTitleText: {
        "fontSize": 32,
        "fontFamily": "AlphaMack AOE",
        "color": colours.darkBlue
    },
    noteCopyText: {
        "fontSize": 18,
        "color": "hsl(0,0%,20%)"
    },
    descriptionTitleView: {
        "marginTop": 10,
        "flex": 1
    },
    descriptionTitleText: {
        "fontSize": 32,
        "fontFamily": "AlphaMack AOE",
        "color": colours.darkBlue
    },
    descriptionCopyText: {
        "fontSize": 18,
        "color": "hsl(0,0%,20%)"
    },
}
