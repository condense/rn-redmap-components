
import React from 'react';
import { View, Animated, Text, ScrollView, SafeAreaView, TouchableOpacity } from 'react-native';
import { UsernameInput, PasswordInput } from './labelled-input';
import { Button } from './button';
import { ConnectionBanner } from './banner';
import { colours } from './theme';

const styles = {
    "container": { 
        "backgroundColor": colours.grey 
    },
    "ButtonContainer": {
        "flexDirection": "row",
        "justifyContent": "space-between",
        "flex": 1,
        "margin": 10
    },
    "Button": {
        "flex": 1
    },
    "HelpTextContainer": {
        "alignItems": "center",
        "paddingTop": 20
    },
    "HelpText": {
        "textAlign": "center",
        "fontSize": 16,
        "color": "hsl(0,0%,30%)"
    },
    "ResetTextContainer": {
        "alignItems": "center",
        "paddingTop": 20
    },
    "ResetText": {
        "textAlign": "center",
        "fontSize": 16,
        "color": "black",
        "textDecorationLine": "underline",
        "textDecorationStyle": "solid",
        "textDecorationColor": "black"
    }
};

const accountStyles = (isHeld) => ({
    view: {
        "backgroundColor": isHeld.interpolate({
            "inputRange": [0, 1],
            "outputRange": ["hsl(205, 90%, 50%)", "hsl(205, 90%, 30%)"]
        }),
        "marginRight": 10,
        "flex": 1,
        "borderRadius": 4,
        "borderWidth": 1,
        "borderColor": "black",
        "paddingVertical": 5,
        "alignItems": "center"
    },
    text: {
        "color": "white",
        "fontSize": 20,
        "fontWeight": "bold"
    }
});

const AccountButton = (isHeld) => {
    const styles = accountStyles(isHeld);
    return (
        <Animated.View style={styles.view}>
            <Text style={styles.text}>
                New Account
            </Text>
        </Animated.View>
    )
};

const loginStyles = (isHeld) => ({
    view: {
        "backgroundColor": isHeld.interpolate({
            "inputRange": [0, 1],
            "outputRange": ["hsl(3, 85%, 53%)", "hsl(3, 85%, 33%)"]
        }),
        "flex": 1,
        "borderRadius": 4,
        "borderWidth": 1,
        "borderColor": "black",
        "paddingVertical": 5,
        "alignItems": "center"
    },
    text: {
        "color": "white",
        "fontSize": 20,
        "fontWeight": "bold"
    }
});

const LoginButton = (isHeld) => {
    const styles = loginStyles(isHeld);
    return (
        <Animated.View style={styles.view}>
            <Text style={styles.text}>
                Sign in
            </Text>
        </Animated.View>
    )
};

export const LoginScreen = ({
    username, password, isInternetReachable, onUsernameChangeText, onPasswordChangeText, onNewAccountPress, onLoginPress, onPasswordResetPress
}) => (
        <ScrollView
            style={styles.container}
            keyboardShouldPersistTaps='handled'>
            <ConnectionBanner isInternetReachable={isInternetReachable} />
            <UsernameInput
                label={"Username"}
                onChangeText={onUsernameChangeText}
                value={username} />
            <PasswordInput
                label={"Password"}
                onChangeText={onPasswordChangeText}
                value={password} />
            <View
                style={styles.ButtonContainer}>
                <Button
                    style={styles.Button}
                    onPress={() => onNewAccountPress()}
                    animatedView={(isHeld) => AccountButton(isHeld)} />
                <Button
                    style={styles.Button}
                    onPress={() => onLoginPress()}
                    animatedView={(isHeld) => LoginButton(isHeld)} />
            </View>
            <View
                style={styles.HelpTextContainer}>
                <Text style={styles.HelpText}>
                    The username should be{"\n"}
                between three and 30 characters:{"\n"}
                letters, numbers and @ , + - _
                {"\n\n"}
                The password - minimum six characters
                </Text>
            </View>
            <View style={styles.ResetTextContainer}>
                <TouchableOpacity
                    onPress={() => onPasswordResetPress()}>
                    <Text style={styles.ResetText}>
                        Forgot password or username?
                    </Text>
                </TouchableOpacity>
            </View>
            <SafeAreaView />
        </ScrollView>
    )
