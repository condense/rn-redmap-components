import React from 'react';
import { ScrollView, View, SafeAreaView } from 'react-native';
import { getFooterHeight, colours } from './theme';

styles = {
    container: {
        backgroundColor: colours.grey
    }
}

export const FormPage = ({ children }) => (
    <ScrollView
        style={styles.container}>
        <View>{children}</View>
        <SafeAreaView />
    </ScrollView>
);

export const FormPageWithFooter = ({ children }) => (
    <ScrollView
        style={styles.container}>
        <View style={{ paddingBottom: getFooterHeight() }}>
            {children}</View>
        <SafeAreaView />
    </ScrollView>
);
