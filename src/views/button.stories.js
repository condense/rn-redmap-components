import React from 'react';
import { storiesOf } from '@storybook/react-native';
import { Button } from './button';
import { View, Text, Animated } from 'react-native';
import { withKnobs, boolean } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

const buttonStories = storiesOf('Buttons', module).addDecorator(withKnobs);

const renderAnimatedValue = (isHeld) => {
    return (
        <Animated.View style={{
            padding: 20,
            backgroundColor: isHeld.interpolate({
                inputRange: [0, 1],
                outputRange: ["white", "hsl(0,0%,90%)"]
            })
        }}>
            <Text>My Button</Text>
        </Animated.View>
    )
}

buttonStories.add('default view2', () => (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Button
            style={null}
            onPress={action("onPress")}
            animatedView={renderAnimatedValue}
            disabled={boolean("disabled", false)}
        />
    </View>
));
