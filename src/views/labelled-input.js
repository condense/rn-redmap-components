import React from 'react';
import { View, TextInput, Text } from 'react-native';
import { Badge } from 'react-native-elements'

const styles = {
    "container": {
        "borderBottomWidth": 1,
        "borderColor": "hsl(0,0%,70%)"
    },
    "textInput": {
        "height": 52,
        "padding": 10,
        "paddingLeft": 100,
        "backgroundColor": "white",
        "textAlign": "right"
    },
    "labelView": {
        "position": "absolute",
        "left": 10,
        "top": 0,
        "height": "100%",
        "justifyContent": "center"
    },
    "labelText": {
        "fontWeight": "bold",
        "fontSize": 16
    },
    "statusView": {
        "position": "absolute",
        "bottom": 20,
        "left": 2
    }
}

export const ErrorBadge = ({hasError}) => {
    if (hasError) {
        return (
            <View style={styles.statusView}>
                <Badge status="error" />
            </View>
        )
    } else {
        return null;
    }
}

const LabelledInput = ({
    label, placeholder, onChangeText, value, hasError, inputProps
}) => {
    return (
        <View
            style={styles.container}>
            <TextInput
                style={styles.textInput}
                placeholder={placeholder}
                defaultValue={value}
                onChangeText={onChangeText}
                {...inputProps}>
            </TextInput>
            <View
                style={styles.labelView}
                pointerEvents={"none"}>
                <Text
                    style={styles.labelText}>
                    {label}
                </Text>
            </View>
            <ErrorBadge hasError={hasError} />
        </View>
    );
}

const usernameInputProps = {
    "autoCapitalize": "none",
    "autoCompleteType": "username",
    "textContentType": "username"
};

const UsernameInput = ({ inputProps, ...props }) => {
    return LabelledInput({ inputProps: { ...usernameInputProps, ...inputProps }, ...props })
}

const passwordInputProps = {
    "secureTextEntry": true,
    "autoCompleteType": "password",
    "textContentType": "password",
    "autoCapitalize": "none"
}

const PasswordInput = ({ inputProps, ...props }) => {
    return LabelledInput({ inputProps: { ...passwordInputProps, ...inputProps }, ...props })
}

const emailInputProps = {
    "autoCapitalize": "none",
    "keyboardType": "email-address",
    "autoCompleteType": "email",
    "textContentType": "emailAddress"
}

const EmailInput = ({ inputProps, ...props }) => {
    return LabelledInput({ inputProps: { ...emailInputProps, ...inputProps }, ...props })
}

const givenNameInputProps = {
    "autoCapitalize": "words",
    "textContentType": "givenName"
}

const GivenNameInput = ({ inputProps, ...props }) => {
    return LabelledInput({ inputProps: { ...givenNameInputProps, ...inputProps }, ...props })
}

const familyNameInputProps = {
    "autoCapitalize": "words",
    "textContentType": "familyName"
}

const FamilyNameInput = ({ inputProps, ...props }) => {
    return LabelledInput({ inputProps: { ...familyNameInputProps, ...inputProps }, ...props })
}

export { LabelledInput, UsernameInput, PasswordInput, EmailInput, FamilyNameInput, GivenNameInput };
