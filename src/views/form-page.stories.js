import React from 'react';
import { storiesOf } from '@storybook/react-native';
import { withKnobs, boolean } from '@storybook/addon-knobs';

import { View } from 'react-native';
import { FormPage, FormPageWithFooter } from './form-page';
import { UsernameInput, PasswordInput, EmailInput, FamilyNameInput, GivenNameInput } from './labelled-input';

const stories = storiesOf('Form Pages', module).addDecorator(withKnobs);

const placeholder = "Placeholder text";
const onChangeText = () => {};
const value = "";
const hasError = false;

stories.add('FormPage', () => (
    <FormPage>
        <UsernameInput {...{ label: "Username", placeholder, onChangeText, value, hasError }} />
        <PasswordInput {...{ label: "Password", placeholder, onChangeText, value, hasError }} />
        <EmailInput {...{ label: "Email", placeholder, onChangeText, value, hasError }} />
        <FamilyNameInput {...{ label: "Family Name", placeholder, onChangeText, value, hasError }} />
        <GivenNameInput {...{ label: "Given Name", placeholder, onChangeText, value, hasError }} />
    </FormPage>
));

stories.add('FormPageWithFooter', () => (
    <FormPageWithFooter>
        <UsernameInput {...{ label: "Username", placeholder, onChangeText, value, hasError }} />
        <PasswordInput {...{ label: "Password", placeholder, onChangeText, value, hasError }} />
        <View style={{height: 1000}} />
        <GivenNameInput {...{ label: "Given Name", placeholder, onChangeText, value, hasError }} />
    </FormPageWithFooter>
));
