
import { Dimensions, Image } from 'react-native';
import { UitabbarBackground } from './images';

export const colours = {
    headerBlue: "#2a4268",
    lightBlue: "#007aff",
    midBlue: "#257abc",
    darkBlue: "#0a2c5d",
    footerBlue: "#1a468f",
    red: "#ee2e23",
    grey: "#eee",
}

export const getDeviceSize = () => {
    const { width, height } = Dimensions.get("window");
    if (width > 400 && height > 700) {
        return "large";
    } else if (width > 350) {
        return "medium";
    } else {
        return "small";
    }
}

const scaleFactors = {
    small:  1,
    medium: 1.2,
    large:  1.5,
};

export const getScaleFactor = () => scaleFactors[getDeviceSize()]

export const footerImage = Image.resolveAssetSource(UitabbarBackground);

export const getFooterHeight = () => {
    const windowDimensions = Dimensions.get("window");
    const footerRatio = footerImage.width / footerImage.height;
    return Math.round(windowDimensions.width / footerRatio);
}
