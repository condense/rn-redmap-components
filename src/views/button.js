import React from 'react';
import { TouchableWithoutFeedback, Animated } from 'react-native';

const Button = ({style, onPress, animatedView, disabled}) => {
    const isHeld = React.useRef(new Animated.Value(0)).current;
    return (
        <TouchableWithoutFeedback
                style={style}
                onPressIn={()=>isHeld.setValue(1)}
                onPressOut={()=>isHeld.setValue(0)}
                onPress={() => onPress()}
                disabled={disabled}>
            {animatedView(isHeld)}
        </TouchableWithoutFeedback>
    );
}

export { Button };
