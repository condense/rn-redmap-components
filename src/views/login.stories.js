import React from 'react';
import { storiesOf } from '@storybook/react-native';
import { withKnobs, text, boolean } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import { LoginScreen } from './login';

const stories = storiesOf('Login Screen', module).addDecorator(withKnobs);

stories.add('LoginScreen', () => (
    <LoginScreen
        username={text("username", "")}
        password={text("password", "")}
        isInternetReachable={boolean("isInternetReachable", true)}
        onUsernameChangeText={action("onUsernameChangeText")}
        onPasswordChangeText={action("onPasswordChangeText")}
        onNewAccountPress={action("onNewAccountPress")}
        onLoginPress={action("onLoginPress")}
    />
));
