import React from 'react';
import { storiesOf } from '@storybook/react-native';
import { SubmittingBanner, OutboxBanner } from './banner';
import { SafeAreaView, View, Text, Animated } from 'react-native';
import { withKnobs, boolean, radios } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

const stories = storiesOf('Banner', module).addDecorator(withKnobs);


stories.add('submitting banner', () => (
    <SafeAreaView>
        <View>
            <SubmittingBanner
                isSubmitting={boolean("isSubmitting", true)}
            />
        </View>
    </SafeAreaView>
));


stories.add('outbox banner', () => (
    <SafeAreaView>
        <View>
            <OutboxBanner outboxState={(radios("outboxState", ["empty", "queued", "submitting"], "empty"))} />
        </View>
    </SafeAreaView>
));
