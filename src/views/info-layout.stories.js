import React from 'react';
import { storiesOf } from '@storybook/react-native';
import { withKnobs, text, number } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import { View } from 'react-native';
import { InfoLink } from './info-link';
import { InfoLayout, InfoHeader, InfoBlock } from './info-layout';
import { safari, LogoImas } from "./images";

const stories = storiesOf('Info Page', module).addDecorator(withKnobs);

stories.add('InfoBlock', () => (
    <View>
        <InfoBlock><InfoHeader>Heading text1</InfoHeader></InfoBlock>
        <InfoBlock><InfoHeader>Heading text2</InfoHeader></InfoBlock>
        <InfoBlock><InfoHeader>Heading text3</InfoHeader></InfoBlock>
    </View>
));

stories.add('InfoHeader', () => (
    <InfoHeader>Heading text</InfoHeader>
));

stories.add('InfoLink', () => (
    <InfoBlock>
        <InfoHeader>Heading and links</InfoHeader>
        <InfoLink
            text="Link without logo"
            icon={safari}
            onPress={action("onPress")}
        />
        <InfoLink
            logo={LogoImas}
            text="Link with logo"
            icon={safari}
            onPress={action("onPress")}
        />
        <InfoLink
            logo={LogoImas}
            text="Link with lots and lots and lots and lots and lots and lots and lots of copy"
            icon={safari}
            onPress={action("onPress")}
        />
    </InfoBlock>
));

stories.add('InfoLayout', () => (
    <InfoLayout onLinkPress={action("onLinkPress")} />
));
