import React, { useState } from 'react';
import { storiesOf } from '@storybook/react-native';
import { withKnobs, boolean, date, radios } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import { HomeLayout } from './home-layout';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { colours } from './theme';

const stories = storiesOf('Home Layout', module) .addDecorator(withKnobs);

const HomeLayoutView = () => (
    <HomeLayout 
        isLoading={(boolean("isLoading", false))}
        isAuthenticated={(boolean("isAuthenticated", false))}
        isRefreshing={(boolean("isRefreshing", false))}
        outboxState={(radios("outboxState", ["empty", "queued", "submitting"], "empty"))}
        showRefreshSpinner={(boolean("showRefreshSpinner", false))}
        isInternetReachable={(boolean("isInternetReachable", true))}
        lastUpdated={date("lastUpdated", new Date("2020-09-01"))}
        onSpotPress={action("onSpotPress")}
        onLogPress={action("onLogPress")}
        onMapPress={action("onMapPress")}
        onHelpPress={action("onHelpPress")}
        onLoginPress={action("onLoginPress")}
        onLogoutPress={action("onLogoutPress")}
        onRefresh={action("onRefresh")}
    />
)

stories.add('Home Layout', HomeLayoutView);


const StatefulRefreshStory = () => {

    const [isRefreshing, setRefreshing] = useState(false);
    const [showRefreshSpinner, setShowRefreshSpinner] = useState(false);
    const [lastUpdated, setLastUpdated] = useState(new Date("2020-09-01"));
    const handleRefreshDone = () => {
        setRefreshing(false);
        setShowRefreshSpinner(false);
        setLastUpdated(new Date());
    }
    const handleRefreshTimeout = () => {
        setShowRefreshSpinner(false);
        setLastUpdated(new Date());
    }
    const handleRefresh = () => {
        if (isRefreshing) return;
        setRefreshing(true);
        setShowRefreshSpinner(true);
        setTimeout(handleRefreshTimeout, 2000)
        setTimeout(handleRefreshDone, 10000)
    }
    return (
        <HomeLayout 
            isLoading={boolean("isLoading", false)}
            isAuthenticated={(boolean("isAuthenticated", false))}
            isRefreshing={isRefreshing}
            showRefreshSpinner={showRefreshSpinner}
            isInternetReachable={(boolean("isInternetReachable", true))}
            lastUpdated={lastUpdated}
            onSpotPress={action("onSpotPress")}
            onLogPress={action("onLogPress")}
            onMapPress={action("onMapPress")}
            onHelpPress={action("onHelpPress")}
            onLoginPress={action("onLoginPress")}
            onLogoutPress={action("onLogoutPress")}
            onRefresh={handleRefresh}
        />
    )
}

stories.add('Refreshing', () => <StatefulRefreshStory />);
