import React from 'react';
import { Text, View, StyleSheet, Image, ImageBackground, TouchableWithoutFeedback, TouchableHighlight, Dimensions } from 'react-native';
import MapView, { Circle } from 'react-native-maps';
import { Marker64 } from './images';

const windowWidth = Dimensions.get("window").width;
const designWidth = 414;
const scale = windowWidth / designWidth;

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        height: "100%",
        width: "100%",
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    calloutContainer: {
        backgroundColor: "white",
        flexDirection: "row",
        borderRadius: 10 * scale,
        paddingVertical: 8 * scale,
    },
    calloutImageView: {
        paddingLeft: 8 * scale,
        paddingRight: 5 * scale,
        paddingVertical: 5 * scale
    },
    calloutImage: {
        width: 42 * scale,
        height: 42 * scale,
    },
    calloutCopyView: {
        paddingTop: 1 * scale,
        paddingLeft: 5 * scale,
        paddingRight: 14 * scale,
        justifyContent: "center"
    },
    calloutCommonNameText: {
        fontSize: 20 * scale,
    },
    calloutSpeciesNameText: {
        paddingTop: 3,
        fontSize: 18 * scale,
        color: "#888888",
    },
    overlayView: {
        position: "absolute",
        top: -72 * scale,
        left: -70 * scale,
        minWidth: 200 * scale, // hack - better if text determines width
    },
    markerView: {
    },
    markerImage: {
        width: 32,
        height: 32,
        resizeMode: "contain"
    }
});

export const Marker = () => (
    <View style={styles.markerView}>
        <Image
            style={styles.markerImage}
            source={Marker64} />
    </View>
);

export const Callout = ({ commonName, speciesName, photoUrl }) => (

    <View style={styles.calloutContainer}>
        <View style={styles.calloutImageView}>
        { photoUrl ?
            <Image
                style={styles.calloutImage}
                source={{ uri: photoUrl }}
                resizeMode={"contain"}
            />
            : null }
        </View> 
        <View style={styles.calloutCopyView}>
            <Text numberOfLines={1} style={styles.calloutCommonNameText}>{commonName}</Text>
            {speciesName ? <Text numberOfLines={1} style={styles.calloutSpeciesNameText}>{speciesName}</Text> : null}
        </View>
    </View>
);

export const SightingMarker = ({
    commonName, speciesName, photoUrl, coordinate, isOpen
}) => (
        <MapView.Marker
            coordinate={coordinate}>
            <Marker />
            { isOpen
                ? <View style={styles.overlayView}>
                    <Callout {...{ commonName, speciesName, photoUrl }} />
                </View>
                : null
            }
        </MapView.Marker>
    )


const defaultRegion = {
    "latitude": -26,
    "longitude": 135,
    "latitudeDelta": 50,
    "longitudeDelta": 50
}

export const SightingAccuracyCircle = ({
    latitude, longitude, radius
}) => (
        <Circle
            center={{ latitude, longitude }}
            radius={radius}
            strokeWidth={1}
            strokeColor={"rgba(0,0,0,0.85)"}
            fillColor={"rgba(255,255,255,0.1)"}
        />
    )

// https://stackoverflow.com/a/7478827
const sightingBoundingBox = ({latitude, longitude, radius}) => {
    const r_earth = 6378000;
    const pi = Math.PI
    const dlat  = (radius / r_earth) * (180 / pi);
    const dlon = (radius / r_earth) * (180 / pi) / Math.cos(latitude * pi/180);
    return [
        {latitude: latitude-dlat, longitude: longitude-dlon},
        {latitude: latitude+dlat, longitude: longitude-dlon},
        {latitude: latitude-dlat, longitude: longitude+dlon},
        {latitude: latitude+dlat, longitude: longitude+dlon},
    ]
} 

export const SightingMap = ({
    sightings, onDetailPress
}) => {
    const [mapRef, setMapRef] = React.useState(null);
    const [selectedSightingIdx, setSelectedSightingIdx] = React.useState(null);
    const handleMarkerPress = (idx) => {
        setSelectedSightingIdx(idx);
        const sighting = sightings[idx];
        const partialCamera = {
            center: {
                latitude: sighting.latitude,
                longitude: sighting.longitude
            }
        }
        mapRef.animateCamera(partialCamera, { duration: 500 })
    }
    const handleLayout = () => {
        const coords = sightings.flatMap(sightingBoundingBox);
        mapRef.fitToCoordinates(
            coords,
            {edgePadding: { top: 50, right: 50, bottom: 150, left: 50 }}
        )
    }
    const handleMapPress = () => setSelectedSightingIdx(null);
    const selectedSighting = selectedSightingIdx != null ? sightings[selectedSightingIdx] : null;
    return (
        <MapView
            ref={setMapRef}
            style={styles.map}
            initialRegion={defaultRegion}
            onPress={handleMapPress}
        >
            {sightings.map((sighting, idx) => (
                <MapView.Marker
                    key={"circle" + idx}
                    coordinate={{ latitude: sighting.latitude, longitude: sighting.longitude }}
                    anchor={{ x: 0.2, y: 1 }}
                    centerOffset={{ x: 10, y: -10 }}
                    onPress={() => handleMarkerPress(idx)}
                    stopPropagation={true}
                    style={{ zIndex: 1 }}
                    onLayout={handleLayout}
                >
                    <Marker />
                </MapView.Marker>

            ))}

            {sightings.map((sighting, idx) => (
                sighting.radius
                    ? <SightingAccuracyCircle
                        key={"accuracy" + idx}
                        latitude={sighting.latitude}
                        longitude={sighting.longitude}
                        radius={sighting.radius}
                    />
                    : null
            ))}

            {selectedSighting
                ?
                <MapView.Marker
                    key={"callout" + selectedSightingIdx}
                    coordinate={{ latitude: selectedSighting.latitude, longitude: selectedSighting.longitude }}
                    anchor={{ x: 0.4, y: 1.7 }} // android
                    centerOffset={{ x: 10, y: -60 }} //ios
                    stopPropagation={true}
                    onPress={() => onDetailPress(selectedSighting.id)}
                    style={{ zIndex: 1000 }} // Workaround in case there's a marker directly behind our callout
                >
                    <Callout
                        commonName={selectedSighting.commonName}
                        speciesName={selectedSighting.speciesName}
                        photoUrl={selectedSighting.photoUrl} />
                </MapView.Marker>
                : null}
        </MapView>
    )
}