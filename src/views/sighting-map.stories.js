import React from 'react';
import { storiesOf } from '@storybook/react-native';
import { View, Text, Animated, Image, StyleSheet, TouchableWithoutFeedback } from 'react-native';
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import { Marker, Callout, SightingMarker, SightingMap, SightingAccuracyCircle } from './sighting-map';

const stories = storiesOf('SightingMap', module).addDecorator(withKnobs);

import MapView, { Circle, Point } from 'react-native-maps';



const species = {
    "id": 3,
    "url": "https://redmap-uat.imas.utas.edu.au/api/species/3/",
    "species_name": "Abudefduf vaigiensis",
    "common_name": "Indo-Pacific sergeant",
    "update_time": "2012-10-14T11:19:24.050000",
    "short_description": "Yellow back and five dark vertical bars on pale body, but no streaks on tail fin. ",
    "description": "The Indo-Pacific sergeant has a yellow back and five dark vertical bars on a pale body, but no streaks on tail fin (like Abudefduf sexfasciatus - which it often schools with).\r\n\r\nLength: Up to 22 cm\r\n",
    "image_credit": "Antonia Cooper",
    "picture_url": "http://redmap-uat.imas.utas.edu.au/media/cache/7b/c3/7bc3370fe77f05be33262739f59a5d08.jpg",
    "distribution_url": "https://geoserver.imas.utas.edu.au/geoserver/redmap/wms?service=WMS&version=1.1.0&request=GetMap&styles=&bbox=1.237199884670033E7%2C-5516608.554242996%2C1.7218876832381703E7%2C-892319.0468662267&srs=EPSG%3A900913&format=image%2Fpng&layers=redmap%3AMB_SPECIES_DISTRIBUTION_REDMAP_VIEW&width=200&height=200&transparent=TRUE&CQL_FILTER=%28ID%3D2%29",
    "category_list": [
        "https://redmap-uat.imas.utas.edu.au/api/category/1/"
    ],
    "category_id_list": [
        1
    ],
    "region_id_list": [
        1
    ],
    "notes": "Log it in Western Australia when you see it south of Rottnest Island\r\n"
}

const sighting =
{
    "id": 3531,
    "url": "https://redmap-uat.imas.utas.edu.au/api/sighting/3531/",
    "species": "https://redmap-uat.imas.utas.edu.au/api/species/43/",
    "species_id": 43,
    "other_species": null,
    "is_published": false,
    "region": "https://redmap-uat.imas.utas.edu.au/api/region/5/",
    "region_id": 5,
    "update_time": "2020-08-30T20:17:52.883000",
    "latitude": -42.893533563654,
    "longitude": 147.366964705288,
    "accuracy": 7,
    "logging_date": "2020-08-30T20:17:52.870000",
    "is_valid_sighting": false,
    "photo_url": "http://redmap-uat.imas.utas.edu.au/media/cache/27/d9/27d90d1a181264da19fcc6e933a80b59.jpg",
    "sighting_date": "2020-08-26T07:09:13.887000",
    "time": 26
}

stories.add('Marker', () =>
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Marker />
    </View>)


stories.add('Callout', () =>
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: "#ccc" }}>
        <Callout
            commonName={species.common_name}
            speciesName={species.species_name}
            photoUrl={sighting.photo_url}
        />
    </View>)

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        height: "100%",
        width: "100%",
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
});

export const SingleSightingMap = () => {
    const [isOpen, setOpen] = React.useState(false);

    console.log("SightingMap2.render", { isOpen })

    const handleMarkerPress = () => {
        console.log("handleMarkerPress")
        setOpen(true)
    }

    const handleMapPress = () => {
        console.log("handleMapPress")
        setOpen(false)
    }
    const latitude = sighting.latitude;
    const longitude = sighting.longitude;

    return (
        <View style={styles.container}>
            <MapView
                style={styles.map}
                initialRegion={{
                    latitude: latitude,
                    longitude: longitude,
                    latitudeDelta: 0.1,
                    longitudeDelta: 0.1,
                }}
                onPress={handleMapPress}
                onMarkerPress={handleMarkerPress}
            >
                <SightingMarker
                    commonName={"Human"}
                    speciesName={"Homo sapiens"}
                    photoUrl={"https://robohash.org/human"}
                    coordinate={{latitude, longitude}}
                    isOpen={isOpen}
                />
                <SightingAccuracyCircle 
                    latitude={latitude}
                    longitude={longitude}
                    radius={number("radius", 100000)}
                />
            </MapView>

        </View>
    )
}

stories.add('SightingMarker', () => (<SingleSightingMap />));

export const NoPhotoSightingMap = () => {
    const [isOpen, setOpen] = React.useState(false);

    console.log("SightingMap2.render", { isOpen })

    const handleMarkerPress = () => {
        console.log("handleMarkerPress")
        setOpen(true)
    }

    const handleMapPress = () => {
        console.log("handleMapPress")
        setOpen(false)
    }
    const latitude = sighting.latitude;
    const longitude = sighting.longitude;

    return (
        <View style={styles.container}>
            <MapView
                style={styles.map}
                initialRegion={{
                    latitude: latitude,
                    longitude: longitude,
                    latitudeDelta: 0.1,
                    longitudeDelta: 0.1,
                }}
                onPress={handleMapPress}
                onMarkerPress={handleMarkerPress}
            >
                <SightingMarker
                    commonName={"Human"}
                    speciesName={"Homo sapiens"}
                    photoUrl={null}
                    coordinate={{latitude, longitude}}
                    isOpen={isOpen}
                />
                <SightingAccuracyCircle 
                    latitude={latitude}
                    longitude={longitude}
                    radius={number("radius", 100000)}
                />
            </MapView>

        </View>
    )
}

stories.add('NoPhotoMarker', () => (<NoPhotoSightingMap />));

stories.add('SightingMap', () => <SightingMap
    sightings={[
        {
            id: 101,
            latitude: -42.8821,
            longitude: 147.3272,
            commonName: species.common_name,
            speciesName: species.species_name,
            photoUrl: sighting.photo_url,
            radius: 100
        },
        {
            id: 202,
            latitude: sighting.latitude - 0.01,
            longitude: sighting.longitude,
            commonName: species.common_name,
            speciesName: species.species_name,
            photoUrl: sighting.photo_url,
            radius: 1000
        },
        {
            id: 202,
            latitude: sighting.latitude + 0.01,
            longitude: sighting.longitude + 0.01,
            commonName: "Other species 1",
            speciesName: null,
            photoUrl: sighting.photo_url,
            radius: 10000
        }
    ]}
    onDetailPress={action("onDetailPress")}
/>)


stories.add('no sightings', () => <SightingMap
    sightings={[]}
    onDetailPress={action("onDetailPress")}
/>)
