import React from 'react';
import { storiesOf } from '@storybook/react-native';
import { withKnobs, text, boolean } from '@storybook/addon-knobs';

import { View } from 'react-native';
import { SpeciesPage } from './species-page';
import { UsernameInput, PasswordInput, EmailInput, FamilyNameInput, GivenNameInput } from './labelled-input';

const stories = storiesOf('Species Pages', module).addDecorator(withKnobs);

const placeholder = "Placeholder text";
const onChangeText = () => { };
const value = "";
const hasError = false;

const species = {
    "id": 3,
    "url": "https://redmap-uat.imas.utas.edu.au/api/species/3/",
    "species_name": "Abudefduf vaigiensis",
    "common_name": "Indo-Pacific sergeant",
    "update_time": "2012-10-14T11:19:24.050000",
    "short_description": "Yellow back and five dark vertical bars on pale body, but no streaks on tail fin. ",
    "description": "The Indo-Pacific sergeant has a yellow back and five dark vertical bars on a pale body, but no streaks on tail fin (like Abudefduf sexfasciatus - which it often schools with).\r\n\r\nLength: Up to 22 cm\r\n",
    "image_credit": "Antonia Cooper",
    "picture_url": "http://redmap-uat.imas.utas.edu.au/media/cache/7b/c3/7bc3370fe77f05be33262739f59a5d08.jpg",
    "distribution_url": "https://geoserver.imas.utas.edu.au/geoserver/redmap/wms?service=WMS&version=1.1.0&request=GetMap&styles=&bbox=1.237199884670033E7%2C-5516608.554242996%2C1.7218876832381703E7%2C-892319.0468662267&srs=EPSG%3A900913&format=image%2Fpng&layers=redmap%3AMB_SPECIES_DISTRIBUTION_REDMAP_VIEW&width=200&height=200&transparent=TRUE&CQL_FILTER=%28ID%3D2%29",
    "category_list": [
        "https://redmap-uat.imas.utas.edu.au/api/category/1/"
    ],
    "category_id_list": [
        1
    ],
    "region_id_list": [
        1
    ],
    "notes": "Log it in Western Australia when you see it south of Rottnest Island\r\n"
}

stories.add('SpeciesPage', () => {

    const showPicture = boolean("showPicture", true);

    return (
        <SpeciesPage
            speciesName={text('speciesName', species.species_name)}
            distributionUrl={species.distribution_url}
            notes={text('notes', species.notes)}
            imageCredit={text('imageCredit', species.image_credit)}
            pictureUrl={showPicture ? species.picture_url : null}
            description={text('description', species.description)}
            commonName={text('commonName', species.common_name)}
        />
    )
});


stories.add('SpeciesPage2', () => {

    return (
        <SpeciesPage
            speciesName={text('speciesName', species.species_name)}
            distributionUrl={species.distribution_url}
            notes={text('notes', species.notes)}
            imageCredit={text('imageCredit', species.image_credit)}
            pictureUrl={"https://redmap-uat.imas.utas.edu.au/media/cache/03/d7/03d7a9eea119d6bfb25cd7d2369464c2.jpg"}
            description={text('description', species.description)}
            commonName={text('commonName', species.common_name)}
        />
    )
});
