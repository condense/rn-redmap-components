import React from 'react';
import { storiesOf } from '@storybook/react-native';
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import { Nav, TabButtons, HomeButton } from './nav';
import { View, Text, Button } from 'react-native';

const stories = storiesOf('Nav', module).addDecorator(withKnobs);



import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { colours } from './views/theme';


function HomeScreen({ navigation }) {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>Home Screen</Text>
            <Button
                title="Go to Details"
                onPress={() => navigation.navigate('Details', {
                    itemId: 86,
                    otherParam: 'anything you want here',
                })}
            />
        </View>
    );
}

function DetailsScreen({ navigation, route }) {
    const { itemId, otherParam } = route.params;
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>Details Screen</Text>
            <Text>itemId: {JSON.stringify(itemId)}</Text>
            <Text>otherParam: {JSON.stringify(otherParam)}</Text>
            <Button
                title="Go to Details... again"
                onPress={() => navigation.push('Details', {
                    itemId: Math.floor(Math.random() * 100),
                })}
            />
            <Button title="Go to Home" onPress={() => navigation.navigate('Home')} />
            <Button title="Go back" onPress={() => navigation.goBack()} />
            <Button
                title="Go back to first screen in stack"
                onPress={() => navigation.popToTop()}
            />
        </View>
    );
}

const Stack = createStackNavigator();


export default function Fundamentals() {
    return (
        <NavigationContainer>
            <Stack.Navigator
                screenOptions={{
                    headerStyle: {
                        backgroundColor: '#f4511e',
                    },
                    headerTintColor: '#fff',
                    headerTitleStyle: {
                        fontWeight: 'bold',
                    },
                }}>
                <Stack.Screen
                    name="Home"
                    options={{
                        title: 'My home'
                    }}>
                    {props => <HomeScreen {...props} extraData={{}} />}
                </Stack.Screen>
                <Stack.Screen
                    name="Details"
                    component={DetailsScreen}
                    options={({ route }) => ({ title: route.params.itemId })} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

stories.add('Fundamentals', () => {
    return (
        <Fundamentals />
    )
});


stories.add('Tab header', () => {

    const homePress = action("HomeButton.onPress");
    const tabPress = action("TabButtons.onPress");
    const selectedIndex = number("selectedIndex", 0)
    
    return (
        <NavigationContainer>
            <Stack.Navigator
                screenOptions={{
                    "headerStyle": {
                        "backgroundColor": colours.headerBlue,
                    },
                    "headerTintColor": "white",
                    "headerTitleAlign": "center",
                    "headerTitleStyle": {
                        "fontFamily": "AlphaMack AOE",
                        "fontWeight": "200",
                        "fontSize": 36
                    }
                }}>
                <Stack.Screen
                    name="Home"
                    options={{
                        headerTitle: () => null,
                        headerLeft: () => <HomeButton onPress={homePress} />,
                        headerRight: () => <TabButtons onPress={tabPress} selectedIndex={selectedIndex} />
                    }}
                    component={View}
                />
            </Stack.Navigator>
        </NavigationContainer>
    )
});