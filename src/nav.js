import React from 'react';
import { View, Text, Dimensions, TouchableOpacity, Image } from 'react-native';
import { ButtonGroup } from 'react-native-elements';
import { HomeIcon } from './views/images';
import { colours } from './views/theme';

const styles = {
    headerLeftIconContainer: {
        "paddingLeft": 12
    },
    headerLeftIconImage: {
        "flex": 0,
        "height": 22,
        "width": 22
    },
}

export const HomeButton = ({
    onPress
}) => (
        <TouchableOpacity
            onPress={() => onPress()}
            style={styles.headerLeftIconContainer}>
            <Image
                source={HomeIcon}
                resizeMode="contain"
                style={styles.headerLeftIconImage}
            />
        </TouchableOpacity>
    )


export const TabButtons = ({
    onPress, selectedIndex,
}) => {
    const window = Dimensions.get('window');
    return (
        <ButtonGroup
            onPress={(idx) => onPress(idx)}
            selectedIndex={selectedIndex}
            buttons={["Map", "Photos"]}
            containerStyle={{
                borderWidth: 1,
                borderColor: "white",
                borderRadius: 9,
                width: window.width - 60,
                height: 30,
            }}
            selectedButtonStyle={{
                backgroundColor: "white"
            }}
            selectedTextStyle={{
                color: colours.headerBlue
            }}
            buttonStyle={{
                backgroundColor: colours.headerBlue,

            }}
            textStyle={{
                color: "white",
                fontSize: 14
            }}
        />
    )
}

export const Nav = () => (
    <View><Text>Nav</Text></View>
)