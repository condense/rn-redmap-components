import { getStorybookUI, configure } from '@storybook/react-native';
import { name as appName } from '../app.json';
import { AppRegistry } from 'react-native';
import './rn-addons';

configure(() => {
  require('./stories');
}, module);

const StorybookUIRoot = getStorybookUI({
    asyncStorage: require('@react-native-community/async-storage').default,
    shouldDisableKeyboardAvoidingView: true
});

AppRegistry.registerComponent(appName, () => StorybookUIRoot);
